<b>[Click here](https://w3id.org/lode/owlapi/https://lab.allmende.io/valueflows/valueflows/-/raw/master/release-doc-in-process/all_vf.TTL) for a web-based formatted view</b> of the Valueflows class and property definitions.

<b>[Click here](https://lab.allmende.io/valueflows/valueflows/-/blob/master/release-doc-in-process/all_vf.TTL) for the source "system of record" turtle (ttl) file</b> for all Valueflows class and property definitions.

*Note*: Because of the way the semantic web works, both of these represent only the Valueflows namespace.  See [Other Namespaces](external-terms.md) for details on the non-Valueflows namespace elements. For complete representations for what is needed for a Valueflows based core economic vocabulary, see [Complete Diagram](uml.md) or [GraphQL Reference](graphql.md).
