# Production Examples

1. [Manufacturing](#manufacturing)
1. [Service](#service)
1. [Workflow production](#workflow-production)
1. [Pack unpack](#pack-unpack)

#### Manufacturing

Simple one-process manufacturing with typical inputs and outputs.

![manufacturing diagram](https://raw.githubusercontent.com/valueflows/valueflows/master/release-doc-in-process/proc-mfg.png)

[import, lang:"yaml"](../../examples/process-manufacturing.yaml)

#### Service

Simple delivery of a service with typical inputs and outputs.

![service diagram](https://raw.githubusercontent.com/valueflows/valueflows/master/release-doc-in-process/proc-svc.png)

[import, lang:"yaml"](../../examples/process-service.yaml)

#### Workflow production

Simple repair process with typical inputs and outputs.  The same economic resource goes into and out of the process(es).

![workflow diagram](https://raw.githubusercontent.com/valueflows/valueflows/master/release-doc-in-process/proc-workflow.png)

[import, lang:"yaml"](../../examples/process-workflow.yaml)

#### Pack unpack

Simple pack and unpack of resources into and out of a container resource.

![pack unpack diagram](https://raw.githubusercontent.com/valueflows/valueflows/master/release-doc-in-process/pack-unpack.png)

[import, lang:"yaml"](../../examples/process-pack-unpack.yaml)
